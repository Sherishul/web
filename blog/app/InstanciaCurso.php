<?php

namespace RedCross;

use Illuminate\Database\Eloquent\Model;

class InstanciaCurso extends Model
{
    protected $fillable = [
        'id', 'rut', 'estadoCurso',
    ];

    public function persona(){
        return $this->belongsTo('RedCross\Persona.php');
    }

    public function curso(){
        return $this->belongsTo('RedCross\Curso.php');
    }
}
