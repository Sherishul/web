<?php

namespace RedCross\Http\Controllers;

use Illuminate\Http\Request;


class crearEventoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function crearEvento(){
        return view ('crearEvento');
    }

}
