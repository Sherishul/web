<?php

namespace RedCross\Http\Controllers;

use Illuminate\Http\Request;
use RedCross\Http\Controllers\Controller;

class EventoController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {

        if($request){

            $query=trim($request->get('searchText'));
            $eventos=DB::table('evento')->where('nombre', 'LIKE', '%'.$query.'%');
        }
        return view('evento.index', ["eventos"=>$eventos, "serchText"=>$query]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crearEvento');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventoFormRequest $request)
    {
        $evento=new Evento;
        $event->nombreEvento=$request->get('nombreEvento');
        $event->descripcion=$request->get('descripcion');
        $event->tipoEvento='1';
        $categoria->save();
        return Redirect:: to('evento.show', ["evento"=>Evento::findOrFail()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
