<?php

namespace RedCross\Http\Controllers;

use Illuminate\Http\Request;


class publicacionesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function publicaciones(){
        return view ('publicaciones');
    }

}
