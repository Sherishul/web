<?php

namespace RedCross\Http\Controllers;

use Illuminate\Http\Request;


class inicioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function inicio(){
        return view ('inicio');
    }

}