<?php

namespace RedCross;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = 'personas';

    protected $fillable = [
        'rutPersona', 'nombrePersona', 'cargo', 'region',
    ];

    public function instaciaE(){
        return $this->belongsToMany('RedCross\Evento.php');
    }

    public function cargo(){
        return $this->belongsTo('RedCross\cargo.php');
    }

    public function intanciaP(){
        return $this->belongsToMany('RedCross\InstanciaCurso.php');
    }
}
