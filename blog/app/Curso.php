<?php

namespace RedCross;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $fillable = [
        'idCurso', 'nombreCurso', 'descripcion',
    ];

    public function instanciaC(){
        return $this->belongsToMany('RedCross\InstanciaCurso.php');
    }
}
