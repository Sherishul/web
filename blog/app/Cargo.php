<?php

namespace RedCross;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $fillable = [
     'id','nombreCurso','descripcion',
    ];

    public function persona(){
        return $this->belongsToMany('RedCross\Persona.php');
    }
}
