<?php

namespace RedCross;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $table='evento';
    protected $primaryKey='idcategoria';
    public $timestamps=false;

    protected $fillable=[
    	'nombreEvento',
    	'descripcion',
    	'tipoEvento'
    ];

    protected $guarded=[

    ];
}
