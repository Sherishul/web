<?php

namespace RedCross;

use Illuminate\Database\Eloquent\Model;

class Ubicacion extends Model
{
    protected $table = 'personas';
    
        protected $fillable = [
            'id', 'rutPersona', 'region', 'comuna', 'calle'
        ];
    
        public function instaciaU(){
            return $this->belongsTo('RedCross\Persona.php');
        }
    
}
