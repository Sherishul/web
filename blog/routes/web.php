<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
	return view('welcome');
});

Route::get('/blog-main', function() {
	return view('blog-main');
});

Route::get('auth/login', function (){
	return view('auth/login');
});

Route::get('login/{service}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{service}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('verificaCorreo', 'Auth\RegisterController@verificaCorreo')->name('verificaCorreo');
Route::get('verify/{email}/{verifyToken}','Auth\RegisterController@envioHecho')->name('envioHecho');

Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
Route::get('/password/reset/{token}/{email}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');

Route::get('/blog-main', 'blogmainController@inicio')->name('blog-main');

Route::get('/miPerfil', 'PerfilController@miPerfil')->name('miPerfil');

Route::get('/publicaciones', 'publicacionesController@publicaciones')->name('publicaciones');

Route::get('/crearEvento', 'crearEventoController@crearEvento')->name('crearEvento');

Route::get('/inicio', 'inicioController@inicio')->name('inicio');

Route::get('/contacto', 'contactoController@inicio')->name('contacto');

Route::get('/verPublicaciones', 'verPublicacionesController@inicio')->name('verPublicaciones');
