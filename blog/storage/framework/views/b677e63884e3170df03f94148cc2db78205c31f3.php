<?php $__env->startSection('content'); ?>

<!--______________MENU LATERAL__________________-->
<div class="container">

    <div class="row profile">
        <div class="col-md-2">
            <div class="profile-sidebar">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="https://i.ytimg.com/vi/US8BmC2ZeBE/hqdefault.jpg" class="img-responsive" alt="">
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        Marcus Doe
                    </div>
                    <div class="profile-usertitle-job">
                        Developer
                    </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="active">
                            <a href="<?php echo e(route('blog-main')); ?>">
                            <i class="glyphicon glyphicon-user"></i>
                            Inicio </a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('publicaciones')); ?>">
                            <i class="glyphicon glyphicon-user"></i>
                            Publicaciones </a>
                        </li>
                        <li >
                            <a href="<?php echo e(route('miPerfil')); ?>">
                            <i class="glyphicon glyphicon-home"></i>
                            Mi Perfil </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                            <i class="glyphicon glyphicon-ok"></i>
                            Cursos </a>
                        </li>
                        <li>
                        <li>
                            <a href="<?php echo e(route('contacto')); ?>">
                            <i class="glyphicon glyphicon-flag"></i>
                            Contacto </a>
                        </li>
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
        </div>

    

            		<!--__________________CONTENIDO___________________-->
            		
            	<?php if(Request::url() === 'http://localhost:8000/blog-main'): ?>
                    <?php $__env->startSection('content1'); ?>
                        <div class="col-md-10">
                        <div class="profile-content">
                        
                         <div class="container">
  <h2>Carousel Example</h2>  
  <div id="myCarousel" class="carousel slide" data-ride="carousel" style="width:900px;">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" >
      <div class="item active">
        <img src="http://www.cruzroja.cl/images/noticias/0_7.jpg" alt="Los Angeles" style="width:900px; height: 250px">
      </div>

      <div class="item">
        <img src="http://www.cruzroja.cl/images/noticias/ISIS_Y_SOLE.jpg" alt="Chicago" style="width:900px; height: 250px">
      </div>
    
      <div class="item">
        <img src="http://www.cruzroja.cl/images/galerias/4_133.jpg" alt="New york" style="width:900px; height: 250px">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

La Cruz Roja Chilena es una institución Chilena de carácter voluntario, dedicada a prestar ayuda humanitaria, que forma parte del Movimiento Internacional de la Cruz Roja y de la Media Luna Roja.
 En este especio podrás encontrar un espacio para realizar colaboraciones e informarte sobre eventos dentro de la comunidad de la cruz roja e interactuar con sus miembros. También puedes sugerir y plantear distintas actividades que creas que pueda ser de importancia para nuestra comunidad. 


                        

                        </div>
                    </div>
                    <?php $__env->stopSection(); ?>
                <?php endif; ?>

                

                    
                   <?php echo $__env->yieldContent('content1'); ?>
                    

                    
           

 </div>
</div>
<!--______________FIN DE MENU LATERAL__________________-->


<!-- Barra lateral menu-->
		


<!-- FIN DE BARRA LATERL MENU-->


<?php $__env->stopSection(); ?>
   
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>