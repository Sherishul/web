<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-heading">Ingresar</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="<?php echo e(route('login')); ?>">
                        <?php echo e(csrf_field()); ?>


                        <?php if(session()->has('alertaregistro')): ?>
                            <div class="panel-body">
                                <div class="alert alert-success"><?php echo e(session('alertaregistro')); ?></div>
                            </div>
                        <?php endif; ?>

                        <?php if(session()->has('alertaregistrocompleto')): ?>
                            <div class="panel-body">
                                <div class="alert alert-success"><?php echo e(session('alertaregistrocompleto')); ?></div>
                            </div>
                        <?php endif; ?>

                        <?php if(session()->has('status')): ?>
                            <div class="panel-body">
                                <div class="alert alert-success"><?php echo e(session('status')); ?></div>
                            </div>
                        <?php endif; ?>

                        <?php if(session()->has('alertanovalidado')): ?>
                            <div class="panel-body">
                                <div class="alert alert-danger"><?php echo e(session('alertanovalidado')); ?></div>
                            </div>
                        <?php endif; ?>

                        <?php if(session()->has('alertanoregistro')): ?>
                            <div class="panel-body">
                                <div class="alert alert-danger"><?php echo e(session('alertanoregistro')); ?></div>
                            </div>
                        <?php endif; ?>

                        <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                            <label for="email" class="col-md-4 control-label">E-Mail </label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" required autofocus>

                                <?php if($errors->has('email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                <?php if($errors->has('password')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>> Recordarme
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i> Ingresar
                                </button>
                                <a class="btn btn-secundary" href="<?php echo e(route('password.request')); ?>">
                                    Recuperar contraseña
                                </a>
                            </div>
                        </div>
                        <div>
                            <center>
                                <a class="btn btn-social btn-google" href='<?php echo e(url("login/google")); ?>'>
                                    <span class="fa fa-google"></span> Ingresa con Google
                                </a>
                                <a class="btn btn-social btn-facebook" href='<?php echo e(url("login/facebook")); ?>'>
                                    <span class="fa fa-facebook"></span> Ingresa con Facebook
                                </a>
                                <a class="btn btn-social btn-twitter" href='<?php echo e(url("login/twitter")); ?>'>
                                    <span class="fa fa-twitter"></span> Ingresa con Twitter
                                </a>
                            </center>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>