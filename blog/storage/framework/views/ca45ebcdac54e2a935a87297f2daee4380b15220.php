<?php $__env->startSection('content'); ?>

<!--______________MENU LATERAL__________________-->
<div class="container">

    <div class="row profile">
        <div class="col-md-2">
            <div class="profile-sidebar">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="https://i.ytimg.com/vi/US8BmC2ZeBE/hqdefault.jpg" class="img-responsive" alt="">
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        Marcus Doe
                    </div>
                    <div class="profile-usertitle-job">
                        Developer
                    </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="active">
                            <a href="<?php echo e(route('blog-main')); ?>">
                            <i class="glyphicon glyphicon-user"></i>
                            Inicio </a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('publicaciones')); ?>">
                            <i class="glyphicon glyphicon-user"></i>
                            Publicaciones </a>
                        </li>
                        <li >
                            <a href="<?php echo e(route('miPerfil')); ?>">
                            <i class="glyphicon glyphicon-home"></i>
                            Mi Perfil </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                            <i class="glyphicon glyphicon-ok"></i>
                            Cursos </a>
                        </li>
                        <li>
                            <a href="#">
                            <i class="glyphicon glyphicon-flag"></i>
                            Quienes somos </a>
                        </li>
                        <li>
                            <a href="#">
                            <i class="glyphicon glyphicon-flag"></i>
                            Contacto </a>
                        </li>
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
        </div>
       
            		<!--__________________CONTENIDO___________________-->
            		<?php echo $__env->yieldContent('content1'); ?>
            		<div class="col-md-10">
                        <div class="profile-content">
              
                        <!--__________________CONTENIDO___________________-->

                        HOLA ESTO ES EL INICIO

                        </div>
                    </div>
                    
           

 </div>
</div>
<!--______________FIN DE MENU LATERAL__________________-->


<!-- Barra lateral menu-->
		


<!-- FIN DE BARRA LATERL MENU-->


<?php $__env->stopSection(); ?>
   
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>