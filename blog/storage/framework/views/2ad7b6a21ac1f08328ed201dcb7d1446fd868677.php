<?php $__env->startSection('content'); ?>

<!-- Barra lateral menu-->

		<div class="col-md-10">
            <div class="profile-content">
			  
            		<!--__________________CONTENIDO___________________-->

            		<nav class="navbar navbar-default">
					  <div class="container-fluid">
						    
						    <ul class="nav navbar-nav">
						      <li><a href="<?php echo e(route('crearEvento')); ?>">CREAR EVENTO</a></li>
						      <li><a href="#">VER PUBLICACIONES</a></li>
						    </ul>
					  </div>
					</nav>
					
					<?php echo $__env->yieldContent('contentPublicacion'); ?>
            </div>
		</div>


<!-- FIN DE BARRA LATERL MENU-->


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>