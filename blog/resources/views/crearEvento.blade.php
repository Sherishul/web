  
@extends('publicaciones')

@section('contentPublicacion')
	<h1>Crear Evento</h1>
	<div class="input-group">
	    <span >Nombre del evento</span>
	    <input id="nombreEvento" type="text" class="form-control" name="nombreEvento" placeholder="Ingrese nombre del evento">
	</div>
	<br>
	<div class="input-group">
	    <span>Descripcion del evento</span>
	    <br>
	    <textarea style="width: 700px" id="descripcionEvento" class="form-control" rows="5"  placeholder="Ingrese la descripción del envento" name="descripcionEvento"></textarea>
	</div>

	<br>
    <a href="#" class="btn btn-success"> Crear evento</a>

	@endsection
