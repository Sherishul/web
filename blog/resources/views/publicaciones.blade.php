@extends('blog-main')

@section('content1')

<!-- Barra lateral menu-->

		<div class="col-md-10">
            <div class="profile-content">
			  
            		<!--__________________CONTENIDO___________________-->

            		<nav class="navbar navbar-default">
					  <div class="container-fluid">
						    
						    <ul class="nav navbar-nav">
						      <li><a href="{{ route('crearEvento') }}">CREAR EVENTO</a></li>
						      <li><a href="{{ route('verPublicaciones') }}">VER PUBLICACIONES</a></li>
						    </ul>
					  </div>
					</nav>
					
					@yield('contentPublicacion')
            </div>
		</div>


<!-- FIN DE BARRA LATERL MENU-->


@endsection
