<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => RedCross\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'google' => [
        'client_id' => '1005802976105-o1366r9iksjc4turako9lj6bthkk7bll.apps.googleusercontent.com',
        'client_secret' => 'o7T94KPboPgJvfJVzw4qbwdB',
        'redirect' => 'http://localhost:8000/login/google/callback',
    ],
 
    'twitter' => [
        'client_id' => 'GLrRkOX9FWozGx8vOEYqmSin5',
        'client_secret' => 'clpaaT1hEXXae6RSFcED5QJSHHd4BgbRWGh72K3BAA16zx1t6a',
        'redirect' => 'http://localhost:8000/login/twitter/callback',
    ],

    'facebook' => [
        'client_id' => '118823655543891',
        'client_secret' => 'd2f036cf87223f4019b4dff7d9c4c095',
        'redirect' => 'http://localhost:8000/login/facebook/callback',
    ],
];
